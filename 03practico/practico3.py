import cv2
import sys


if(len(sys.argv) > 1): #se fija en elemento [1] de argv. ese es el nombre del archivo a procesar. el elemento [0] es el nombre del programa
    filename= sys.argv[1] #guarda en la variable filename el nombre del video
else:
    print("Pass a filename as first argument") #hay que pasarle un archivo como argumento
    sys.exit(0) #sale del programa

cap= cv2.VideoCapture(filename) #Abrimos el archivo de video y lo guardamos en cap. Lo laburamos desde aca.

fourcc = cv2.VideoWriter_fourcc("M", "J", "P", "G")#Formato del codec de video. En este caso es MJPG. Puede ser XVID. Lo paso caracter por caracter
video_framerate = int(cap.get(cv2.CAP_PROP_FPS)) #obtengo los fps del video
width = int(cap.get(cv2. CAP_PROP_FRAME_WIDTH )) #obtengo el ancho del video
height = int(cap.get(cv2. CAP_PROP_FRAME_HEIGHT )) #obtengo el alto del video
print(video_framerate,height,width)#imprimo para verificar los valores obtenidos
framesize=(width, height)#armo una tupla con el tamanio del video
out = cv2.VideoWriter("output.avi", fourcc, video_framerate, framesize, 0 )#a los parametros que obtuve antes, antes de usarlos en esta funcion los casteo a int
                                                                            #cv2.VideoWriter es para creaer un archivo de video "output.avi" con la codificacion y 
                                                                            # tamaño obtenidos anteriormente

delay = (1000/video_framerate)#Invertimos los fps para obtener el periodo de tiempo entre frames. 
                              #Lo multiplicamos por 1000 para obtenerlo en mili segundos



while(cap.isOpened()): #devuelve True si el video ha sido inicializado correctamente
    ret, frame = cap.read() #lee el valor de cap (el video que abrimos) y devuelve dos valores. ret para verificar si esta bien y en frame el cuadro del 
                            #video en ese instante
    if ret is True:
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)#le aplico un filtro gris al frame y lo guardo en gray
        out.write(gray) #Escribimos la imagen leida en el archivo de salida
        cv2.imshow("Image gray", gray) #Mostramos la imagen leida en pantalla
        if cv2.waitKey(int(delay)) & 0xFF == ord("q"):#espera hasta que cambie la imagen (un video es una sucesion de imagenes) y hace un and bit a bit para
                                                     #"limpiar" el caracter que yo elija para salir. la funcion ord es de python. si se aprieta "q" sale
            break
    else:
        break

cap.release() #limpia la memoria alojada en cap
out.release() #limpia la meomria alojada en out
cv2.destroyAllWindows()